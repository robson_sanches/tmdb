import "./assets/scss/app.scss";

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import VueAxios from 'vue-axios'
import VueRouter from 'vue-router'
import App from './App.vue'
import { LayoutPlugin, BootstrapVue, IconsPlugin, CollapsePlugin } from 'bootstrap-vue'
import store from './store/index.js';
import router from './routes/index.js';
import { library } from '@fortawesome/fontawesome-svg-core'
import { faChevronRight, faChevronDown } from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'

library.add(faChevronRight,faChevronDown)

Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(VueAxios, axios)
Vue.use(Vuex)
Vue.use(VueRouter)
Vue.use(LayoutPlugin)
Vue.use(BootstrapVue)
Vue.use(IconsPlugin)
Vue.use(CollapsePlugin)

import AppNav from './views/layout/Nav.vue';
import AppContent from './views/layout/Content.vue';
import AppMain from './views/layout/Main.vue';
import AppSidebar from './views/layout/Sidebar.vue';
import AppFooter from './views/layout/Footer.vue';

Vue.component('app-nav', AppNav);
Vue.component('app-content', AppContent);
Vue.component('app-main', AppMain);
Vue.component('app-sidebar', AppSidebar);
Vue.component('app-footer', AppFooter);

Vue.config.productionTip = false

window.axios = require('axios');
window.axios.defaults.baseURL = 'https://api.themoviedb.org/3';
window.axios.defaults.params = {}
window.axios.defaults.params['api_key'] = 'd8d8b0d88c30e805e1a05deb28c205ab';
window.axios.defaults.params['language'] = 'pt-BR';

const DEFAULT_TITLE = 'The Movie Database';
router.afterEach((to) => {
    Vue.nextTick(() => {
        document.title = to.meta.title || DEFAULT_TITLE;
    });
});

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
