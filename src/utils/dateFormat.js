const meses = [
    "Janeiro",
    "Fevereiro",
    "Março",
    "Abril",
    "Maio",
    "Junho",
    "Julho",
    "Agosto",
    "Setembro",
    "Outubro",
    "Novembro",
    "Dezembro"
];

export default function(str){
    var partes = str.split('-').map(Number);
    var data = new Date(partes[0], partes[1] - 1, partes[2]);
    var mes = meses[data.getMonth()];
    var ano = data.getFullYear();

    return [data.getDate(), mes.slice(0, 3).toLowerCase(), ano].join(' de ');
}