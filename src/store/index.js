import Vue from 'vue'
import Vuex from 'vuex'

window.Vue = Vue

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        app: {
            config: {}
        }
    },
    mutations: {
        setState(state,payload){
            state[payload.name] = payload.value
        },
        appConfig(state,payload){
            state.app.config = payload
        }
    },
    getters: {
        appConfig: state => {
            return state.app.config
        }
    }
});

