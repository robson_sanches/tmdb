import VueRouter from 'vue-router';

import MovieList from '../views/movie/List.vue';
import MovieShow from '../views/movie/Show.vue';

let routes = [
    {
        path: '/',
        name: 'movie.list',
        component: MovieList,
        meta: {
            title: 'The Movie Database'
        }
    },
    {
        path: '/movie/:id',
        name: 'movie.show',
        component: MovieShow,
        meta: {
            title: 'Movie - The Movie Database'
        }
    }
];

export default new VueRouter({
    // history: true,
    // mode: 'history',
    // base: "/",
    routes
});