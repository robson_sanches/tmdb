module.exports = {
    pages: {
      index: {
        entry: 'src/main.js',
        title: 'The Movie Database'
      }
    },
    css: {
      loaderOptions: {
        sass: {
          additionalData: `
            @import "@/assets/scss/_variables.scss";
          `
        }
      }
    },
    publicPath:''
  }